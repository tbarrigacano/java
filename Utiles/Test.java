
import java.util.Scanner;
import java.util.Random;

public class Test {

    public static void main(String[] args) {
        char[] caracteres = new char[] { 'a', 'j', 'h', 'o', 'b' };
        int[] numeros = new int[] { 2, 65, 87, 6, 123 };
        int[] nums = { 1, 4, 23, 16, 12 };
        int[] nums2 = { 1, 4, 23, 16, 12, 59, 8, 62 };
        String email = "adds@g+mail.com";
        String password = "hH3dddddddd";
        Scanner lector = new Scanner(System.in);
        String palabraClave = "";
        String palabra = "";
        String palabraEncriptada = "";
        int numeroOpcion;

        System.out.println(Utiles.semana(3, "en"));
        System.out.println(Utiles.mes(1, "es"));
        System.out.println(Utiles.mayor(caracteres));
        System.out.println(Utiles.mayor(numeros));
        Utiles.muestraArray(nums, ">>>");
        System.out.println();
        Utiles.muestraArray(nums2, "/");
        System.out.println();
        Utiles.muestraArray(Utiles.pares(nums2), "''");
        System.out.println();
        System.out.println("Password: " + Utiles.password(10));
        System.out.println("El Email " + email + " es: " + Utiles.verificaEmail(email));
        System.out.println("El Password " + password + " es: " + Utiles.verificaPassword(password));
        while (true) {
            System.out.println("Desea encriptar o desencriptar:?(1/2)Para salir (3)");
            numeroOpcion = lector.nextInt();
            if (numeroOpcion == 1) {
                System.out.println("Introduce la palabra que desea encriptar:");
                palabra = lector.next();
                System.out.println("Introduce la palabra que desea usar para encriptar:");
                palabraClave = lector.next();
                System.out.println("Esta es la palabra ya encriptada: " + Utiles.encripta(palabraClave, palabra));

            } else if (numeroOpcion == 2) {
                System.out.println("Introduce la palabra que desea desencriptar:");
                palabra = lector.next();
                System.out.println("Introduce la palabra que desea usar para desencriptar:");
                palabraClave = lector.next();
                System.out.println("Esta es la palabra desencriptada: " + Utiles.desencripta(palabraClave, palabra));
            } else if (numeroOpcion == 3) {
                break;
            }
        }

    }
}
