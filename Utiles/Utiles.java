public class Utiles {
    static String[] semana_es = new String[] { "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado",
            "Domingo" };
    static String[] semana_ca = new String[] { "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte",
            "Diumenge" };
    static String[] semana_en = new String[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday",
            "Sunday" };
    static String[] mes_es = new String[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
            "Septiembre", "Octubre", "Noviembre", "Diciembre" };
    static String[] mes_ca = new String[] { "Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost",
            "Septembre", "Octubre", "Novembre", "Desembre" };
    static String[] mes_en = new String[] { "January", "February", "March", "April", "May", "June", "July", "August",
            "September", "October", "November", "December" };
    static String[] alfabeto = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "k", "j", "l", "m", "n", "o", "p",
            "q", "r", "s", "t", "u", "v", "w", "x", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B",
            "C", "D", "E", "F", "G", "H", "K", "J", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X",
            "Z" };

    static String semana(int numDia, String idioma) {
        numDia = numDia - 1;
        String nombreDia = "Idioma o dia seleccionado no existe";
        if (numDia <= 6 && numDia >= 0) {
            if (idioma == "es" || idioma == "ES") {
                nombreDia = semana_es[numDia];
            } else if (idioma == "ca" || idioma == "CA") {
                nombreDia = semana_ca[numDia];
            } else if (idioma == "en" || idioma == "EN") {
                nombreDia = semana_en[numDia];
            }
        }
        return nombreDia;
    }

    static String mes(int numMes, String idioma) {
        numMes = numMes - 1;
        String nombreMes = "Idioma o dia seleccionado no existe";
        if (numMes <= 11 && numMes >= 0) {
            if (idioma == "es" || idioma == "ES") {
                nombreMes = mes_es[numMes];
            } else if (idioma == "ca" || idioma == "CA") {
                nombreMes = mes_ca[numMes];
            } else if (idioma == "en" || idioma == "EN") {
                nombreMes = mes_en[numMes];
            }
        }
        return nombreMes;
    }

    static int mayor(int[] cosas) {
        int numeroMayor = cosas[0];
        for (int s : cosas) {
            if (s > numeroMayor) {
                numeroMayor = s;
            }
        }
        return numeroMayor;
    }

    static char mayor(char[] cosas) {
        char caracterMayor = cosas[0];
        for (char s : cosas) {
            if (s > caracterMayor) {
                caracterMayor = s;
            }
        }
        return caracterMayor;
    }

    static void muestraArray(int[] lista, String marca) {
        int contador = 0;
        for (int i : lista) {
            System.out.print(i);
            if (contador != lista.length - 1) {
                System.out.print(marca);
            }
            contador++;
        }
    }

    static int[] pares(int[] lista) {
        int contador = 0;
        for (int i : lista) {
            if (i % 2 == 0) {
                contador++;
            }
        }
        int[] listaPares = new int[contador];
        contador = 0;
        for (int i : lista) {
            if (i % 2 == 0) {
                listaPares[contador] = i;
                contador++;
            }
        }
        return listaPares;
    }

    static String password(int numero) {
        int contador = 0;
        String contrasenya = "";
        if (numero >= 8) {
            while (true) {

                do {
                    contrasenya = contrasenya + alfabeto[(int) (Math.random() * alfabeto.length)];
                    contador++;
                } while (contador != numero);
                contador = 0;
                if (contrasenya.matches(".*[a-z]+.*") && contrasenya.matches(".*[A-Z]+.*")
                        && contrasenya.matches(".*[0-9]+.*")) {
                    break;
                }
                contrasenya = "";

            }
        } else {
            return "El tamaño debe ser minimo de 8 caracteres";
        }
        return contrasenya;
    }

    static boolean verificaEmail(String email) {
        boolean estado = false;
        if (email.matches("...*@(gmail|hotmail).(com|es)$")) {
            estado = true;
        }
        return estado;
    }

    static boolean verificaPassword(String contrasenya) {
        boolean estado = false;
        if (contrasenya.length() >= 8) {
            if (contrasenya.matches(".*[a-z]+.*") && contrasenya.matches(".*[A-Z]+.*")
                    && contrasenya.matches(".*[0-9]+.*")) {
                estado = true;
            }
        }
        return estado;
    }

    static String encripta(String palabraClave, String palabra) {
        char[] caracteres = palabra.toCharArray();
        char[] clave = palabraClave.toCharArray();
        char[] palabraEncriptada = new char[caracteres.length];
        int contador = 0;
        int contador2 = 0;
        for (char i : caracteres) {
            if (contador <= clave.length - 1) {
                int caracterNumero = clave[contador];
                System.out.println(caracterNumero);
                int letra = i;
                System.out.println("valor letra " + letra + " valor caracter numero " + caracterNumero);
                caracterNumero = caracterNumero + i;
                System.out.println("suma de los dos caracteres en forma de int: " + caracterNumero);
                palabraEncriptada[contador2] = (char) caracterNumero;
                System.out.println("letra en forma de char: " + palabraEncriptada[contador2]);
            } else {
                contador = 0;
                palabraEncriptada[contador2] = (char) (i + clave[contador]);
            }
            contador++;
            contador2++;
        }
        return String.valueOf(palabraEncriptada);
    }

    static String desencripta(String palabraClave, String palabra) {
        char[] caracteres = palabra.toCharArray();
        char[] clave = palabraClave.toCharArray();
        char[] palabraEncriptada = new char[caracteres.length];
        int contador = 0;
        int contador2 = 0;
        for (char i : caracteres) {
            if (contador <= clave.length - 1) {
                int caracterNumero = clave[contador];
                System.out.println(caracterNumero);
                int letra = i;
                System.out.println("valor letra " + letra + " valor caracter numero " + caracterNumero);
                caracterNumero = i-caracterNumero;
                System.out.println("suma de los dos caracteres en forma de int: " + caracterNumero);
                palabraEncriptada[contador2] = (char) caracterNumero;
                System.out.println("letra en forma de char: " + palabraEncriptada[contador2]);
            } else {
                contador = 0;
                palabraEncriptada[contador2] = (char) (i - clave[contador]);
            }
            contador++;
            contador2++;
        }
        return String.valueOf(palabraEncriptada);
    }

}