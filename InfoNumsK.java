package app01;

import java.util.Scanner;

class InfoNumsK {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int num = 1;
        int contador=0;
        float media=0f;
        int mayor=0;
        int menor=0;
        do {
            System.out.println("Entra un num: ");
            try {
                num = keyboard.nextInt();
                if (num>mayor){
                    mayor =num;
                }else if(num<menor && num!=0){
                    menor=num;
                }else if (mayor==0){
                    mayor =num;
                }else if (menor==0){
                    menor=num;
                }
                media=media+num;
                contador++;
            } catch (Exception e) {
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            }
        } while (num != 0);

        media=media/(contador-1);
        System.out.println("El mayor numero es "+mayor+", el menor es "+menor+" y la media es "+String.format("%.2f", media));
        keyboard.close();
    }
}