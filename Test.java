package app01;

public class Test {
    public static void main(String[] args) {

        // Persona marta = new Persona();
        // marta.nombre = "Marta";
        // marta.edad = 20;
        // Persona isabel = new Persona();
        // isabel.nombre = "Isabel";
        // isabel.edad = 22;

        // marta.saluda(isabel);
        // isabel.saluda(marta);
        Libreria l1 = new Libreria();
        int miNumero = 2;
        System.out.println(l1.doblar(miNumero));
        System.out.println(miNumero);

        Persona pedro = new Persona();
        pedro.setNombre("pedro");
        pedro.setEdad(25);
        System.out.println(l1.doblaEdad(pedro));
        System.out.println(pedro.getEdad());
    }
}