package app01;

class InfoNums {
    public static void main(String[] args) {
        int mayor=Integer.parseInt(args[0]);
        float media=0f;
        int menor=Integer.parseInt(args[0]);
        int contador=0;
        for (String s : args){
            int num = Integer.parseInt(s);
            if (num>mayor){
                mayor =num;
            }else if(num<menor){
                menor=num;
            }
            media=media+num;
            contador++;
        }
        media=media/contador;
        System.out.println("El mayor numero es "+mayor+", el menor es "+menor+" y la media es "+String.format("%.2f", media));
    }
}
