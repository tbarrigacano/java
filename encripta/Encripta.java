package encripta;

public class Encripta {
	public static String encripta1(String palabra) {
		char letra;
		char[] palabraPartida = new char[palabra.length()];
		for (int i = 0; i < palabra.length(); i++) {
			letra = palabra.charAt(i);
			switch (letra) {
			case 'a':
				letra = '1';
				break;
			case 'e':
				letra = '2';
				break;
			case 'i':
				letra = '3';
				break;
			case 'o':
				letra = '4';
				break;
			case 'u':
				letra = '5';
				break;
			default:
				// code block
			}
			palabraPartida[i] = letra;
		}
		palabra = String.valueOf(palabraPartida);
		return palabra;
	}

	public static String encripta2(String palabra) {
		char letra;
		char[] palabraPartida = new char[palabra.length()];
		for (int i = 0; i < palabra.length(); i++) {
			letra = palabra.charAt(i);
			letra = (letra=='z') ? 'a' : (char)(letra+1);
			palabraPartida[i] = letra;
		}
		palabra = String.valueOf(palabraPartida);
		return palabra;
	}
	
	public static String desencripta1(String palabra) {
		char letra;
		char[] palabraPartida = new char[palabra.length()];
		for (int i = 0; i < palabra.length(); i++) {
			letra = palabra.charAt(i);
			switch (letra) {
			case '1':
				letra = 'a';
				break;
			case '2':
				letra = 'e';
				break;
			case '3':
				letra = 'i';
				break;
			case '4':
				letra = 'o';
				break;
			case '5':
				letra = 'u';
				break;
			default:
				// code block
			}
			palabraPartida[i] = letra;
		}
		palabra = String.valueOf(palabraPartida);
		return palabra;
	}
	public static String desencripta2(String palabra) {
		char letra;
		char[] palabraPartida = new char[palabra.length()];
		for (int i = 0; i < palabra.length(); i++) {
			letra = palabra.charAt(i);
			letra = (letra=='a') ? 'z' : (char)(letra-1);
			palabraPartida[i] = letra;
		}
		palabra = String.valueOf(palabraPartida);
		return palabra;
	}
}
