package com.codesplai.vgames;

public class Platforms{

    public int id_plataforma;
    public String plataforma;

    public Platforms (int id, String nombre){
        this.id_plataforma=id;
        this.plataforma=nombre;
    }
    public Platforms(String nombre){
        this.plataforma=nombre;
    }
}