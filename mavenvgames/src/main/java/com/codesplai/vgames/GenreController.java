package com.codesplai.vgames;

import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class GenreController {

    // getAll devuelve todos los registros de la tabla
    public static List<Genre> getAll(){
        List<Genre> listaGenres = new ArrayList<Genre>();
        String sql = "select * from generos";

		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Genre u = new Genre(
                    rs.getInt("id_genero"),
                    rs.getString("genre")
                );
                listaGenres.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaGenres;

    }

    //insertar nuevo genero
    public static void create(Genre genero) {
        String sql = "INSERT INTO generos (genre) VALUES (?)";
     
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)
                ) {
                        
            pstmt.setString(1, genero.genre);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //eliminar editor
    public static String eliminar(int id_genero) {
        String sql = "DELETE from generos where id_genero="+id_genero;
        String sql2 = "select * from generos where id_genero="+id_genero;
        String resul ="No hay ningun registro con este id";
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()
                ) {
                ResultSet rs = stmt.executeQuery(sql2);
                if (rs.first()){
                    pstmt.executeUpdate();
                    resul = "Genero con este id: "+id_genero+" eliminado correctamente";
                    return resul;
                }        
                return resul;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return resul;
    }
    //actualizar genero
    public static String actualizar(int id_genero,String genre) {
        String sql = "UPDATE generos SET genre = (?) where id_genero="+id_genero;
        String sql2 = "select * from generos where id_genero="+id_genero;
        
        String resul ="No hay ningun registro con este id";
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()
                ) {
                ResultSet rs = stmt.executeQuery(sql2);
                if (rs.first()){
                    pstmt.setString(1, genre);
                    pstmt.executeUpdate();
                    resul = "Genero con este id: "+id_genero+" modificado correctamente";
                    return resul;
                }        
                return resul;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return resul;
    }

}