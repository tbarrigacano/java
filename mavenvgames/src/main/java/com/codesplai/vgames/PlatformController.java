package com.codesplai.vgames;

import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class PlatformController {

    // getAll devuelve todos los registros de la tabla
    public static List<Platforms> getAll(){
        List<Platforms> listaPlatforms = new ArrayList<Platforms>();
        String sql = "select * from plataformas";

		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Platforms u= new Platforms(
                    rs.getInt("id_plataforma"),
                    rs.getString("plataforma")
                );
                listaPlatforms.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaPlatforms;

    }
    //insertar nueva plataforma
    public static void create(Platforms plataforma) {
        String sql = "INSERT INTO plataformas (plataforma) VALUES (?)";
     
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)
                ) {
                        
            pstmt.setString(1, plataforma.plataforma);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //eliminar plataforma
    public static String eliminar(int id_plataforma) {
        String sql = "DELETE from plataformas where id_plataforma="+id_plataforma;
        String sql2 = "select * from plataformas where id_plataforma="+id_plataforma;
        String resul ="No hay ningun registro con este id";
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()
                ) {
                ResultSet rs = stmt.executeQuery(sql2);
                if (rs.first()){
                    pstmt.executeUpdate();
                    resul = "Plataforma con este id: "+id_plataforma+" eliminado correctamente";
                    return resul;
                }        
                return resul;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return resul;
    }

    //actualizar plataforma
    public static String actualizar(int id_plataforma,String plataforma) {
        String sql = "UPDATE plataformas SET plataforma = (?) where id_plataforma="+id_plataforma;
        String sql2 = "select * from plataformas where id_plataforma="+id_plataforma;
        
        String resul ="No hay ningun registro con este id";
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()
                ) {
                ResultSet rs = stmt.executeQuery(sql2);
                if (rs.first()){
                    pstmt.setString(1, plataforma);
                    pstmt.executeUpdate();
                    resul = "Plataforma con este id: "+id_plataforma+" modificado correctamente";
                    return resul;
                }        
                return resul;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return resul;
    }
}