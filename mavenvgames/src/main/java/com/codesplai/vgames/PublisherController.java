package com.codesplai.vgames;

import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class PublisherController {

    // getAll devuelve todos los registros de la tabla
    public static List<Publisher> getAll(){
        List<Publisher> listaPublisher = new ArrayList<Publisher>();
        String sql = "select * from editores";

		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Publisher u= new Publisher(
                    rs.getInt("id_editores"),
                    rs.getString("publisher")
                );
                listaPublisher.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaPublisher;

    }
    //insertar nuevo editor
    public static void create(Publisher publi) {
        String sql = "INSERT INTO editores (publisher) VALUES (?)";
     
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)
                ) {
                        
            pstmt.setString(1, publi.publisher);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //eliminar editor
    public static String eliminar(int id_editores) {
        String sql = "DELETE from editores where id_editores="+id_editores;
        String sql2 = "select * from editores where id_editores="+id_editores;
        String resul ="No hay ningun registro con este id";
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()
                ) {
                ResultSet rs = stmt.executeQuery(sql2);
                if (rs.first()){
                    pstmt.executeUpdate();
                    resul = "Editor con este id: "+id_editores+" eliminado correctamente";
                    return resul;
                }        
                return resul;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return resul;
    }

    //actualizar editor
    public static String actualizar(int id_editores,String publisher) {
        String sql = "UPDATE editores SET publisher = (?) where id_editores="+id_editores;
        String sql2 = "select * from editores where id_editores="+id_editores;
        
        String resul ="No hay ningun registro con este id";
        try (   Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()
                ) {
                ResultSet rs = stmt.executeQuery(sql2);
                if (rs.first()){
                    pstmt.setString(1, publisher);
                    pstmt.executeUpdate();
                    resul = "Editor con este id: "+id_editores+" modificado correctamente";
                    return resul;
                }        
                return resul;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return resul;
    }
}