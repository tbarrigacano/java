package com.codesplai.vgames;

public class Publisher{

    public int id_editores;
    public String publisher;

    public Publisher (int id, String nombre){
        this.id_editores=id;
        this.publisher=nombre;
    }

    public Publisher(String nombre){
        this.publisher=nombre;
    }
}