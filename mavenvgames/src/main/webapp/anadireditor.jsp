<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.codesplai.vgames.*" %>

<%

    String editor="";
    if (request.getParameter("editor")!=null){
        editor =request.getParameter("editor");
        Publisher pub = new Publisher(editor);
        PublisherController.create(pub);
    }
%>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="index.jsp">Index</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav navbar-dark bg-primary">
      <a class="nav-item nav-link" href="genres.jsp">Generos</a>
      <a class="nav-item nav-link" href="publishers.jsp">Editores</a>
      <a class="nav-item nav-link" href="platforms.jsp" >Plataformas</a>
    </div>
  </div>
</nav>    
<div class="container">
    <div class="row">
        <div class="col md-4">
<% if (request.getParameter("editor")==null){ %>
<form action="anadireditor.jsp">
<input name="editor" type="text" class="form-control" placeholder="Editor a añadir" >
<button type="submit" class="btn btn-primary">Enviar</button>
</form>
<% } else { %>
<div class="alert alert-success" role="alert" id="alerta">
  <h4 class="alert-heading">Enhorabuena!</h4>
  <p>Editor añadido correctamente</p>
  <hr>
</div>
<a href="index.jsp"><button type="button" class="btn btn-info">Volver al Index</button></a>
<% } %>
        </div>
    </div>
</div>

</body>
</html>