<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.codesplai.vgames.*" %>



<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style>
    .btn {
      margin: 10px;
    }
  </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="index.jsp">Index</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav navbar-dark bg-primary">
      <a class="nav-item nav-link" href="genres.jsp">Generos</a>
      <a class="nav-item nav-link" href="publishers.jsp">Editores</a>
      <a class="nav-item nav-link" href="platforms.jsp" >Plataformas</a>
    </div>
  </div>
</nav>    
<a href="anadirgenero.jsp"><button type="button" class="btn btn-success">Añadir genero</button></a>
<a href="anadirplataforma.jsp"><button type="button" class="btn btn-success">Añadir plataforma</button></a>
<a href="anadireditor.jsp"><button type="button" class="btn btn-success">Añadir editor</button></a>
<br>
<a href="eliminargenero.jsp"><button type="button" class="btn btn-danger">Eliminar genero</button></a>
<a href="eliminarplataforma.jsp"><button type="button" class="btn btn-danger">Eliminar plataforma</button></a>
<a href="eliminareditor.jsp"><button type="button" class="btn btn-danger">Eliminar editor</button></a>
<br>
<a href="actualizargenero.jsp"><button type="button" class="btn btn-warning">Actualizar genero</button></a>
<a href="actualizarplataforma.jsp"><button type="button" class="btn btn-warning">Actualizar plataforma</button></a>
<a href="actualizareditor.jsp"><button type="button" class="btn btn-warning">Actualizar editor</button></a>

</body>
</html>