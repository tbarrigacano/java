import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.Scanner;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Motos {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        
        String marcaMoto;
        File fin = new File("motos.csv");
        File wrMarca;

        // List<String> provincias = new ArrayList<String>();
        while (true){
            System.out.println("Escribe la marca que desea buscar: ");
            marcaMoto= lector.next();
            if (marcaMoto!=null){
                 wrMarca = new File(marcaMoto + ".csv");
                break;
            }
        }
        Set<String> marcaMotos = new TreeSet<String>();
        try (InputStreamReader fr = new InputStreamReader(new FileInputStream(fin), "UTF8");
                BufferedReader br = new BufferedReader(fr);
                FileWriter fwMarca = new FileWriter(wrMarca);
                BufferedWriter bwMarca = new BufferedWriter(fwMarca);
                ) {
            String linea;
            do {
                linea = br.readLine();
                if (linea != null && linea.toLowerCase().contains(marcaMoto)) {
                    bwMarca.write(linea);
                    marcaMotos.add(linea);
                    bwMarca.newLine();
                }
            } while (linea != null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // for (String marca : marcaMotos) {
        //     System.out.println(marca);
        // }
    }
}
