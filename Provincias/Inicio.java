import java.util.ArrayList;
import java.util.Collections;

public class Inicio {
    public static void main(String[] args) {
        String[] provincias;
        ArrayList<String> provinciasNuevas= new ArrayList<>();
        int contador=0;
        provincias = Datos.provincias();
        for (String ciudad : provincias){
            if (!provinciasNuevas.contains(ciudad)){
                provinciasNuevas.add(ciudad);
                contador++;
            }
        }
        Collections.sort(provinciasNuevas);
        System.out.println("Se han añadido un total de "+contador+" ciudades nuevas.");
        System.out.println(provinciasNuevas);
        
    }
}