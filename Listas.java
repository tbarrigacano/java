
public class Listas {
	public static void main(String[] args) {
		int[] lista_numeros = { 23, 4, 55, 61, 28, 73, 42, 9 };

		String ciudad = "Constantinopla";
		for (int i = 0; i < ciudad.length(); i++) {
			System.out.print(ciudad.charAt(i));
			if (i != ciudad.length() - 1) {
				System.out.print("-");
			}
		}
		System.out.println();

		for (int i = ciudad.length() - 1; i > -1; i--) {
			System.out.print(ciudad.charAt(i));
			if (i != 0) {
				System.out.print("-");
			}
		}
		for (int numero : lista_numeros) {
			System.out.println(numero);
		}

	}
}
