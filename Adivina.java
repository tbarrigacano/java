package app01;

import java.util.Scanner;
import java.util.Random;

class Adivina {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        Random random = new Random();
        int incognita = random.nextInt(50) + 1;
        int total = 0;
        int num = 1;
        do {
            System.out.println("Entra un num: ");
            try {
                num = keyboard.nextInt();
                total++;
                if (num>incognita){
                    System.out.println("El numero que buscas es menor.");
                }else {
                    System.out.println("El numero que buscas es mayor.");
                }
            } catch (Exception e) {
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            }
        } while (num != incognita);

        System.out.println("El numero secreto era "+incognita+ " y has necesitado "+total+" oportunidades.");
        keyboard.close();
    }
}