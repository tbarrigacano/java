package app01;


public class Persona {
    String nombre;
    int edad;

    void saluda() {
        System.out.println("Hola me llamo "+this.nombre);
    }
    void saluda(Persona otra){
        int diferenciEdad;
        if (this.edad>otra.edad){
            diferenciEdad=this.edad-otra.edad;
            System.out.println("Hola me llamo "+this.nombre+" y soy mayor que "+otra.nombre+" y tengo "+diferenciEdad+" mas");
            
        }
        else {
            diferenciEdad=otra.edad-this.edad;
            System.out.println("Hola me llamo "+this.nombre+" y soy mas joven que "+otra.nombre+" y tengo "+diferenciEdad+" menos");
            
        }

    }
    

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }
    
}
