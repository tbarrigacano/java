import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;

public class Juegos {

    public static Jugador jugador1;
    public static Jugador jugador2;
    public static int partidas;
    public static Scanner keyboard = new Scanner(System.in);
    public static String[] ppt = { "Piedra", "Papel", "Tijera" };
    public static int[] rojos = {1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36};
    public static int[] negros ={2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35};
    static Random rand = new Random();

    /**
     * pide nombre de jugador, 1 o 2 crea objeto "Jugador" y lo asigna a jugador1 o
     * jugador2
     */
    public static void pideJugador(int numJugador) {
        // pedimos nombre de jugador
        String pregunta = String.format("Nombre para el jugador %d: ", numJugador);
        System.out.println(pregunta);
        String nombre = keyboard.next();
        Jugador j = new Jugador(nombre);

        if (numJugador == 1) {
            Juegos.jugador1 = j;
        } else {
            Juegos.jugador2 = j;
        }
    }

    /**
     * muestra menu de juegos y pide opción
     */
    public static void menu() {
        System.out.println("*******************");
        System.out.println("JUEGOS DISPONIBLES:");
        System.out.println("*******************");
        System.out.println("1: Cara o Cruz");
        System.out.println("2: Piedra-Papel-Tijera");
        System.out.println("3: Ruleta");
        System.out.println("*******************");
        // pedimos opcion de juego, comprobando validez
        int opcion = 0;
        do {
            System.out.print("Introduce juego: ");
            opcion = keyboard.nextInt();
        } while (opcion < 1 || opcion > 3);

        switch (opcion) {
        case 1:
            Juegos.caraCruz();
            break;
        case 2:
            Juegos.piedraPapelTijera();
            break;
        case 3:
            Juegos.ruleta();
            break;
        default:
            break;
        }
    }

    /**
     * juego caraCruz
     */
    public static void caraCruz() {
        Juegos.pideJugador(1); // solo interviene un jugador en este juego
        Juegos.partidas = 5; // partidas por defecto en este juego

        System.out.println();
        System.out.println("CARA O CRUZ:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        Random rnd = new Random();
        // bucle de partidas
        for (int i = 0; i < Juegos.partidas; i++) {
            System.out.print("Cara (C) o cruz(X) ?  ");
            // pedimos apuesta aunque no se utiliza
            String apuesta = keyboard.next();
            // sea cual sea la probabilidad es un 50%...
            // obtenemos un boolean aleatorio
            boolean ganador = rnd.nextBoolean();
            if (ganador) {
                System.out.println(" Has acertado!");
                Juegos.jugador1.ganadas++;
            } else {
                System.out.println(" Lo siento...");
            }
            Juegos.jugador1.partidas++;
        }
        // creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas.", Juegos.jugador1.nombre,
                Juegos.jugador1.partidas, Juegos.jugador1.ganadas);
        System.out.println(resumen);
    }

    /**
     * juego Piedra Papel o Tijera
     */
    public static void piedraPapelTijera() {
        Juegos.pideJugador(1);
        Juegos.partidas = 5;

        System.out.println();
        System.out.println("PIEDRA PAPEL O TIJERA:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        String apuesta;
        for (int i = 0; i < Juegos.partidas; i++) {
            System.out.print("Piedra, Papel o Tijera?");
            // pedimos apuesta aunque no se utiliza

            while (true) {
                apuesta = keyboard.next();
                if (apuesta.equalsIgnoreCase("Piedra") || apuesta.equalsIgnoreCase("piedra")
                        || apuesta.equalsIgnoreCase("Papel") || apuesta.equalsIgnoreCase("papel")
                        || apuesta.equalsIgnoreCase("Tijera") || apuesta.equalsIgnoreCase("tijera")) {
                    break;
                }
                System.out.println("La opcion no es valida, porfavor escriba una opcion valida(Piedra,Papel o Tijera)");
            }

            // sea cual sea la probabilidad es un 50%...
            // obtenemos un boolean aleatorio
            
            int numAleatorio = rand.nextInt(3);
            String opcionMaquina = ppt[numAleatorio];
            if (apuesta.equalsIgnoreCase(opcionMaquina)) {
                System.out.println("La maquina ha escogido " + opcionMaquina);
                System.out.println(" Has empatado!");
                Juegos.jugador1.empates++;
            } else if (apuesta.equalsIgnoreCase("tijera") && opcionMaquina.equalsIgnoreCase("papel")) {
                System.out.println("La maquina ha escogido " + opcionMaquina);
                System.out.println(" Muy bien! Has ganado!");
                Juegos.jugador1.ganadas++;
            } else if (apuesta.equalsIgnoreCase("papel") && opcionMaquina.equalsIgnoreCase("piedra")) {
                System.out.println("La maquina ha escogido " + opcionMaquina);
                System.out.println(" Muy bien! Has ganado!");
                Juegos.jugador1.ganadas++;
            } else if (apuesta.equalsIgnoreCase("piedra") && opcionMaquina.equalsIgnoreCase("tijera")) {
                System.out.println("La maquina ha escogido " + opcionMaquina);
                System.out.println(" Muy bien! Has ganado!");
                Juegos.jugador1.ganadas++;
            } else {
                System.out.println("La maquina ha escogido " + opcionMaquina);
                System.out.println(" Ohhhhh, lo siento has perdido! ");
            }
            Juegos.jugador1.partidas++;
        }
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas, %d perdidas.", Juegos.jugador1.nombre,
                Juegos.jugador1.partidas, Juegos.jugador1.ganadas, Juegos.jugador1.empates);
        System.out.println(resumen);
    }

    /**
     * juego RULETA
     */
    public static void ruleta() {
        Juegos.pideJugador(1);
        Juegos.partidas = 5;

        System.out.println();
        System.out.println("RULETA:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        int saldoAJugar;
        int apuestaRN;
        String rn="";
        int apuestaPI;
        String pi="";
        int apuestaFP;
        String fp="";
        int saldo = 100;
        for (int i = 0; i < Juegos.partidas; i++) {
            System.out.println("Tu saldo actual es de " + saldo+ " euros.");
            System.out.println("Cuanto dinero desea apostar?");
            while (true) {
                try {
                    saldoAJugar = keyboard.nextInt();
                    if (saldoAJugar <= saldo) {
                        saldo = saldo - saldoAJugar;
                        break;
                    }
                    System.out.println("No dispones de tanto saldo.");
                } catch (Exception e) {
                    System.out.println("Debes introducir una cantidad numerica.");
                    saldoAJugar = 0;
                    keyboard.next();
                }

            }

            // APUESTAS A ROJO O NEGRO
            System.out.println("Cuanto dinero desea apostar a Rojo/Negro?");
            while (true) {
                try {
                    apuestaRN = keyboard.nextInt();
                    if (apuestaRN == 0) {
                        break;
                    }
                    if (apuestaRN <= saldoAJugar) {
                        saldoAJugar = saldoAJugar - apuestaRN;
                        System.out.println("Desea apostar a Rojo(r) o Negro(n):");
                        while (true) {
                            rn = keyboard.next();
                            if (rn.equalsIgnoreCase("r") || rn.equalsIgnoreCase("n")) {
                                break;
                            }
                            System.out.println("Elige una opcion correcta.");
                        }
                        break;
                    }
                    System.out.println("No dispones de tanto saldo para apostar.");
                } catch (Exception e) {
                    System.out.println("Debes introducir una cantidad numerica.");
                    keyboard.next();
                }
            }

            // APUESTAS A PAR IMPAR
            System.out.println("Cuanto dinero desea apostar a Par/Impar?");
            System.out.printf("Te quedan %d euros para apostar.", saldoAJugar);
            while (true) {
                try {
                    apuestaPI = keyboard.nextInt();
                    if (apuestaPI == 0) {
                        break;
                    }
                    if (apuestaPI <= saldoAJugar) {
                        saldoAJugar = saldoAJugar - apuestaPI;
                        System.out.println("Desea apostar a Par(p) o Impar(i):");
                        while (true) {
                            pi = keyboard.next();
                            if (pi.equalsIgnoreCase("p") || pi.equalsIgnoreCase("i")) {
                                break;
                            }
                            System.out.println("Elige una opcion correcta.");

                        }
                        break;
                    }
                    System.out.println("No dispones de tanto saldo para apostar.");
                } catch (Exception e) {
                    keyboard.next();
                    System.out.println("Debes introducir una cantidad numerica.");
                }
            }

            // APUESTAS A FALTA PASA
            System.out.println("Cuanto dinero desea apostar a Falta/Pasa?");
            System.out.printf("Te quedan %d euros para apostar.", saldoAJugar);
            while (true) {
                try {
                    apuestaFP = keyboard.nextInt();
                    if (apuestaFP == 0) {
                        break;
                    }
                    if (apuestaFP <= saldoAJugar) {
                        saldoAJugar = saldoAJugar - apuestaFP;
                        System.out.println("Desea apostar a Falta(f) o Pasa(p):");
                        while (true) {
                            fp = keyboard.next();
                            if (fp.equalsIgnoreCase("f") || fp.equalsIgnoreCase("p")) {
                                break;
                            }
                            System.out.println("Elige una opcion correcta.");
                        }
                        break;
                    }
                    System.out.println("No dispones de tanto saldo para apostar.");
                } catch (Exception e) {
                    keyboard.next();
                    System.out.println("Debes introducir una cantidad numerica.");
                }
            }

            //GENERACION DEL NUMERO ALEATORIO DE LA RULETA.
            int numAleatorio = rand.nextInt(36);
            System.out.println("Ha salido el numero "+numAleatorio);
            if (numAleatorio==0){
                System.out.println("Has perdido todas las apuestas. Ha salido el numero 0.");
            }else {
                if (Arrays.asList(rojos).contains(numAleatorio)){
                    if(rn.equalsIgnoreCase("r") && apuestaRN!=0){
                        System.out.println("Enhorabuena el numero es rojo y has ganado "+(apuestaRN*2)+" euros!!");
                        saldo= saldo + (apuestaRN*2);
                    }
                }
                if (numAleatorio%2==0 && pi.equalsIgnoreCase("p") && apuestaPI!=0){
                    System.out.println("Enhorabuena el numero es par y has ganado "+(apuestaPI*2)+" euros!!");
                    saldo = saldo+(apuestaPI*2);
                }
                if (numAleatorio<=18 && fp.equalsIgnoreCase("f") && apuestaFP!=0){
                    System.out.println("Enhorabuena el numero falta y has ganado "+(apuestaFP*2)+" euros!!");
                    saldo = saldo+(apuestaFP*2);
                }
                saldo = saldo +saldoAJugar;
            }
            if (saldo==0){
                System.out.println("Game Over pringao! Lo has perdido todo!");
            }

        }
    }

}
