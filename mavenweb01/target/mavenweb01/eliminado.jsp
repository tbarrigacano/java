
<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.codesplai.javaweb.Miweb" %>
<%@page import="com.codesplai.javaweb.PersonaController" %>
<%@page import="com.codesplai.javaweb.Persona" %>
<%
    String ids =  request.getParameter("numero");
    int id = Integer.parseInt(ids);
    boolean respuesta = PersonaController.eliminar(id);
    String mensaje = "Persona no encontrada";
    if (respuesta){
        mensaje = "Persona con id: "+id+" ha sido borrada.";
    }
%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
    <title>Ejercicios HMTL/CSS</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>
    <a href="index.jsp">Volver</a>
    <h1>Pagina de eliminacion</h1>
    <br>
    <h2><%= mensaje %></h2>
    <hr>
</body>
</html>