package com.codesplai.javaweb;

import java.util.ArrayList;
import java.util.List;

public class PersonaController{
    public static List<Persona> agenda = new ArrayList<Persona>();

    public static void inicializa(){
        agenda.clear();
        agenda.add(new Persona("Maria","maria@gmail.com",1));
        agenda.add(new Persona("Pepe","pepe@gmail.com",2));
        agenda.add(new Persona("Jose","jose@gmail.com",3));
        agenda.add(new Persona("Alicia","alicia@gmail.com",4));
    }

    public static Persona getById(int idaencontrar){
        for (Persona p: agenda){
            if (p.id==idaencontrar){
                return p;
            }
        }
        return null;
    }

    public static boolean eliminar(int idABorrar){
        for (Persona p: agenda){
            if (p.id==idABorrar){
                agenda.remove(p);
                return true;
            }
        }
        return false;
    }
    public static boolean anadir(int id,String nombre,String email){
        Persona nueva = new Persona(nombre,email,id);
        agenda.add(nueva);
        return true;
    }

}