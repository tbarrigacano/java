<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.codesplai.javaweb.Miweb" %>
<%@page import="com.codesplai.javaweb.PersonaController" %>
<%
    int numeroPersonas = PersonaController.agenda.size();
    if (numeroPersonas==0){
        PersonaController.inicializa();
    }
    String tabla_html = Miweb.tablaPersonas();
%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
    <title>Ejercicios HMTL/CSS</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>
    <h1>Lista de agenda</h1>
    <br>
    <hr>
    <%= tabla_html %>
    <hr>
    <a href="anadir.jsp">Añadir</button>
    <a href="eliminar.jsp">Eliminar</button>
</body>
</html>