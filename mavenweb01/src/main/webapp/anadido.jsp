
<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.codesplai.javaweb.Miweb" %>
<%@page import="com.codesplai.javaweb.PersonaController" %>
<%@page import="com.codesplai.javaweb.Persona" %>
<%
    Persona ultimaPersona = PersonaController.agenda.get(PersonaController.agenda.size() - 1);
    int numeroPersonas = ultimaPersona.id;
    numeroPersonas++;
    String nombre = request.getParameter("nombre");
    String email = request.getParameter("email");

    boolean respuesta = PersonaController.anadir(numeroPersonas,nombre,email);
    String mensaje = "Persona no añadida";
    if (respuesta){
        mensaje = "Persona con id: "+numeroPersonas+" ha sido añadida.";
    }
%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
    <title>Ejercicios HMTL/CSS</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>
    <a href="index.jsp">Volver</a>
    <h1>Pagina de añadido</h1>
    <br>
    <h2><%= mensaje %></h2>
    <hr>
</body>
</html>