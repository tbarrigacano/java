<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="main.java.com.codesplai.javaweb.Tictactoe" %>

<%
    int[] id;
    String posiciones;
    if (request.getParameter("posiciones")==null){
        id=Tictactoe.inicializar();
    }else {
        posiciones = request.getParameter("posiciones");
        for(int i=0; i<posiciones.length(); i++){
            int numero=(int)posiciones.charAt(i);
            id[i] = numero;
            i++;  
        }
    }

%>

<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        .cel{
            height: 150px;
            width: 150px;
            
        }
        .celda0{
            background-color: gray;
        }
        .celda1{
            background-color: red;
        }
        .celda2{
            background-color: blue;
        }
        .mar {
            height: 150px;
            width: 150px;
            margin: 2px;
            display: inline-block;
        }
        
    </style>
</head>

<body>
    <div class="container">
        <div>
            <div class="mar">
                <div class="cel <%= (id[0] == 1)? "celda1" : (id[0] == 2)? "celda2" : "celda0" %>" id="0"></div>
            </div>
            <div class="mar">
                <div class="cel <%= (id[1] == 1)? "celda1" : (id[1] == 2)? "celda2" : "celda0" %>" id="1"></div>
            </div>
            <div class="mar">
                <div class="cel <%= (id[2] == 1)? "celda1" : (id[2] == 2)? "celda2" : "celda0" %>" id="2"></div>
            </div>
        </div>
        <div>
            <div class="mar">
                <div class="cel <%= (id[3] == 1)? "celda1" : (id[3] == 2)? "celda2" : "celda0" %>" id="3"></div>  
            </div>
            <div class="mar">
                <div class="cel <%= (id[4] == 1)? "celda1" : (id[4] == 2)? "celda2" : "celda0" %>" id="4"></div>
            </div>
            <div class="mar">
                <div class="cel <%= (id[5] == 1)? "celda1" : (id[5] == 2)? "celda2" : "celda0" %>" id="5"></div>
            </div>
        </div>
        <div>
            <div class="mar">
                <div class="cel <%= (id[6] == 1)? "celda1" : (id[6] == 2)? "celda2" : "celda0" %>" id="6"></div>
            </div>
            <div class="mar">
                <div class="cel <%= (id[7] == 1)? "celda1" : (id[7] == 2)? "celda2" : "celda0" %>" id="7"></div>
            </div>
            <div class="mar">
                <div class="cel <%= (id[8] == 1)? "celda1" : (id[8] == 2)? "celda2" : "celda0" %>" id="8"></div>
            </div>
        </div>

    </div>
     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).on("click", ".cel", function () {
            var ids[] = {0,0,0,0,0,0,0,0,0};
            var num=0;
            
            
            var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');
                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                    }
                }
            };
            var idSeleccionado = $(this).attr("id");
            var jugador = getUrlParameter('jug');
            if (jugador==null){
                jugador="1";
            }else if(jugador="1"){
                jugador="2";
            }else {
                jugador="1";
            }
            if (jugador=="1"){
                $( "#"+idSeleccionado ).addClass( "celda1" );
                $( "#"+idSeleccionado ).removeClass( "celda0" );
            }else{
                $( "#"+idSeleccionado ).addClass( "celda2" );
                $( "#"+idSeleccionado ).removeClass( "celda0" );
            }
            

            if ($("#0").hasClass("celda0")) {
                ids[0]=0;
            }else if ($("#0").hasClass("celda1")){
                ids[0]=1;
            }else {
                ids[0]=2;
            }
            if ($("#1").hasClass("celda0")) {
                ids[1]=0;
            }else if ($("#1").hasClass("celda1")){
                ids[1]=1;
            }else {
                ids[1]=2;
            }
            if ($("#2").hasClass("celda0")) {
                ids[2]=0;
            }else if ($("#2").hasClass("celda1")){
                ids[2]=1;
            }else {
                ids[2]=2;
            }
            if ($("#3").hasClass("celda0")) {
                ids[3]=0;
            }else if ($("#3").hasClass("celda1")){
                ids[3]=1;
            }else {
                ids[3]=2;
            }
            if ($("#4").hasClass("celda0")) {
                ids[4]=0;
            }else if ($("#4").hasClass("celda1")){
                ids[4]=1;
            }else {
                ids[4]=2;
            }
            if ($("#5").hasClass("celda0")) {
                ids[5]=0;
            }else if ($("#5").hasClass("celda1")){
                ids[5]=1;
            }else {
                ids[5]=2;
            }
            if ($("#6").hasClass("celda0")) {
                ids[6]=0;
            }else if ($("#6").hasClass("celda1")){
                ids[6]=1;
            }else {
                ids[6]=2;
            }
            if ($("#7").hasClass("celda0")) {
                ids[7]=0;
            }else if ($("#7").hasClass("celda1")){
                ids[7]=1;
            }else {
                ids[7]=2;
            }
            if ($("#8").hasClass("celda0")) {
                ids[8]=0;
            }else if ($("#8").hasClass("celda1")){
                ids[8]=1;
            }else {
                ids[8]=2;
            }
            var envio = "?posiciones="+ids.join()+"&jug="+jugador;
            function myFunction() {
                location.replace("index.jsp"+envio);
            }
            myFunction();
        });
    </script>
</body> 
  
</html>